#!/bin/bash

CLI="/usr/local/bin/yowsup-cli"

echo "Make sure to have the file 'mydetails' setup. After that, press any key to continue"
read VOIDVAR

python $CLI registration -E android --config mydetails --requestcode sms

echo "Check your phone for the registration code via Text. It should be in this format: xxx-xxx."
echo "Insert the code:"
read CODE

python $CLI registration -E android --config mydetails --register $CODE

echo "Get the password from the above output and fill up 'mysettings.py' file. HAVE FUN!"
