# -*- coding: utf-8 -*-
import os, subprocess, time, re
from yowsup.layers                                     import YowLayer
from yowsup.layers.interface                           import YowInterfaceLayer, ProtocolEntityCallback
from yowsup.layers.protocol_messages.protocolentities  import TextMessageProtocolEntity
from yowsup.layers.protocol_receipts.protocolentities  import OutgoingReceiptProtocolEntity
from yowsup.layers.protocol_acks.protocolentities      import OutgoingAckProtocolEntity
from mysettings	import *

ap = set(allowedPersons)

# Message on Start Up
startcommand='yowsup-cli demos -l %s:%s -s %s "*[INFO] System Started*: `uptime`" 2>&1 > /dev/null ' % (phone, password, destphone)
subprocess.call(startcommand , shell=True) 



def sendMessage(self, messageProtocolEntity, msg):
   outgoingMessageProtocolEntity = TextMessageProtocolEntity(
      ''+msg+'',
      to = messageProtocolEntity.getFrom())
   self.toLower(outgoingMessageProtocolEntity)

class EchoLayer(YowInterfaceLayer):
    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):
        #send receipt otherwise we keep receiving the same message over and over
        
        if True:
            receipt = OutgoingReceiptProtocolEntity(messageProtocolEntity.getId(), messageProtocolEntity.getFrom(), 'read', messageProtocolEntity.getParticipant())
            self.toLower(receipt)

        if messageProtocolEntity.getFrom(False) in ap:
             message_body = messageProtocolEntity.getBody().lower().strip(' \t\r\n\0')
             #print (message_body)
             
             # Local System Control
             if 'help' in (message_body):
		msg='Commands available:\nrestart <device>\nuptime\ndf\nlast\nrouter\nkodi start\nkodi stop\nupgrade raspbxino'
                sendMessage(self, messageProtocolEntity, msg)
	     #elif 'reboot' in (message_body):
             #   result=subprocess.check_output(["sudo", "reboot"])
             #   msg='reboot: '+result+''
             #   sendMessage(self, messageProtocolEntity, msg)
             elif 'uptime' in (message_body):
                result=subprocess.check_output(["uptime"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'df' in (message_body):
                result=subprocess.check_output(["df", "-h"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'last' in (message_body):
                result=subprocess.check_output(["last"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'router' in (message_body):
                result=subprocess.check_output(["/usr/lib/nagios/plugins/check_router_speed"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'kodi stop' in (message_body):
                result=subprocess.check_output(["sudo", "manage_kodi", "off"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'kodi start' in (message_body):
                result=subprocess.check_output(["sudo", "manage_kodi", "on"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             elif 'upgrade raspbxino' in (message_body):
                result=subprocess.check_output(["sudo", "upgrade_raspbxino"])
                msg=''+result+''
                sendMessage(self, messageProtocolEntity, msg)
             
             # Reboots Control		
             elif message_body.startswith('restart'):
                cmd = message_body.split('restart', 1)[1]
                if 'skyhub' in (cmd):
		   result=subprocess.check_output(["sudo", "restart_device", "skyhub"])
                   msg=''+result+''
                   sendMessage(self, messageProtocolEntity, msg)
                elif 'asus8uk' in (cmd):
		   result=subprocess.check_output(["sudo", "restart_device", "asus8uk"])
                   msg=''+result+''
                   sendMessage(self, messageProtocolEntity, msg)
		elif 'raspberrino' in (cmd):
		   result=subprocess.check_output(["sudo", "restart_device", "raspberrino"])
                   msg=''+result+''
                   sendMessage(self, messageProtocolEntity, msg)
		elif 'raspbxino' in (cmd):
                   result=subprocess.check_output(["sudo", "restart_device", "raspbxino"])
                   msg=''+result+''
                   sendMessage(self, messageProtocolEntity, msg)
                else:
                   msg='Usage: restart (skyhub|asus8uk|raspberrino|raspbxino)'
                   sendMessage(self, messageProtocolEntity, msg)
             
#             # Torrent Control
#             elif 'scnsrctv' in (message_body): 
#                result=subprocess.check_output(["lynx -dump ''http://www.scnsrc.me/category/tv/'' |awk -F\/ '/lime/ {print $6}'"], shell=True)
#                msg=''+result+''
#                sendMessage(self, messageProtocolEntity, msg)
#            
#             elif 'scnsrc' in (message_body):
#                result=subprocess.check_output(["lynx -dump ''http://www.scnsrc.me/category/films/'' |awk -F\/ '/lime/ {print $6}'"], shell=True)
#                msg=''+result+''
#                sendMessage(self, messageProtocolEntity, msg)
#             
#	     # TV Control
#             elif 'tv' in (message_body):
#                body = message_body.replace('tv ', '')
#                result=subprocess.check_output(["tv-ctrl", ''+body+''])
#                msg=''+result+''
#                sendMessage(self, messageProtocolEntity, msg)
#	     
             else:
                msg='Command '+messageProtocolEntity.getBody()+' unknown.\nUse: help'
                sendMessage(self, messageProtocolEntity, msg)

        else:
	     # Report
             msg='** Alert**  \nSender: '+messageProtocolEntity.getFrom()+' '+messageProtocolEntity.getBody()+''
             outgoingMessageProtocolEntity = TextMessageProtocolEntity(
                ''+msg+'',
                to = '%s@s.whatsapp.net' % destphone )
             self.toLower(outgoingMessageProtocolEntity)
             
             # Reply
             msg='You are *NOT* allowed to contact me. This has been reported.'
             sendMessage(self, messageProtocolEntity, msg)
             
    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        ack = OutgoingAckProtocolEntity(entity.getId(), "receipt", entity.getType(), entity.getFrom())
        self.toLower(ack)
